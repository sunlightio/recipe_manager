import sys
from subprocess import Popen, PIPE
import argparse
import subprocess
import time
import uuid
import os.path
import json
import yaml
from helpers import *


clusterID = sys.argv[1]
playbookFile = sys.argv[2]
logfile = openLogFile(clusterID)
if "cleanup" not in playbookFile:
    deploymentTag = sys.argv[3]
    deploymentID = sys.argv[4]
    exeEnvInventoryFile = sys.argv[5]
    dependentDeploymentID = sys.argv[6]
    limitedHost = sys.argv[7]
    if not deploymentTag:
        if not limitedHost:
            process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + clusterID + '.yml', '-i', exeEnvInventoryFile, playbookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)
        else:
            process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + clusterID + '.yml', '-i', exeEnvInventoryFile, playbookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID, '-l', limitedHost + ',sunlight_platform,infrastructure'],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)

    else:
        if not limitedHost:
            process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + clusterID + '.yml', '-i', exeEnvInventoryFile, playbookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID, '--tags', deploymentTag],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)
        else:
            process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + clusterID + '.yml', '-i', exeEnvInventoryFile, playbookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID, '--tags', deploymentTag, '-l', limitedHost + ',sunlight_platform,infrastructure'],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)

else:
    process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + clusterID + '.yml', playbookFile],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)
while True:
    return_code = process.poll()
    if return_code is not None:
        logfile.write("Return Code: " + str(return_code) + "\n")
        if return_code != 0:
            if "cleanup" in playbookFile:
                update_deployment_status_by_cluster(clusterID, "Error")
            else:
                deploymentID = sys.argv[4]
                update_deployment_status(deploymentID, "Error")
        else:
            if "cleanup" in playbookFile:
                remove_all_deployment(clusterID)
            else:
                deploymentID = sys.argv[4]
                update_deployment_status(deploymentID, "Completed")
        break

