import json
import yaml
from subprocess import Popen, PIPE
import argparse
import subprocess
import time
import os.path
from shutil import copyfile
from datetime import datetime

def execute_local(command):
    p = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = p.communicate()
    rc = p.returncode
    return out.decode('utf-8'), err, rc

def openLogFile(cluster_id):
    deploymentDataDir = "deployment/"
    deploymentLogFile = deploymentDataDir + cluster_id + ".log"
    logfile = open(deploymentLogFile, "a")
    return logfile

def read_recipe_info():
    recipeIndexFile = "recipeIndex.yml"
    if os.path.isfile(recipeIndexFile):
        with open(recipeIndexFile) as recipeIndexFileOpener:
            parsed_recipe_file = yaml.load(recipeIndexFileOpener, Loader=yaml.FullLoader)
            recipes = parsed_recipe_file.get("recipeIndex")
            return recipes
    else:
        return None

def read_deployment_info():
    deploymentInfoFile = "./deployment.json"
    if os.path.isfile(deploymentInfoFile):
        with open(deploymentInfoFile) as deploymentFile:
            deploymentInfo = json.load(deploymentFile)
            return deploymentInfo
    else:
        return None

def update_deployment_status(deployment_id, status):
    deploymentInfo = read_deployment_info()
    if deploymentInfo is None:
        return 1
    for index, deployment in enumerate(deploymentInfo['deploymentList']):
        if deployment['id'] == deployment_id:
            deploymentInfo['deploymentList'][index].update({'status': status})
    with open('deployment.json', 'w') as deploymentFile:
        json.dump(deploymentInfo, deploymentFile, sort_keys=True, indent=4)
    return

def update_deployment_status_by_cluster(cluster_id, status):
    deploymentInfo = read_deployment_info()
    if deploymentInfo is None:
        return 1
    for deployment in deploymentInfo['deploymentList']:
        if deployment['cluster_id'] == cluster_id:
            deployment.update({'status': status})
    with open('deployment.json', 'w') as deploymentFile:
        json.dump(deploymentInfo, deploymentFile, sort_keys=True, indent=4)
    return

def remove_all_deployment(cluster_id):
    deploymentInfo = read_deployment_info()
    for deployment in list(deploymentInfo['deploymentList']):
        if deployment['cluster_id'] == cluster_id:
            deploymentInfo['deploymentList'].remove(deployment)
    with open('deployment.json', 'w') as deploymentFile:
        json.dump(deploymentInfo, deploymentFile, sort_keys=True, indent=4)
    return

def generate_infrastructure_inventory(cluster_id):
    cmd = "curl -X GET localhost:8383/clusters/" + cluster_id
    out, err, rc = execute_local(cmd)
    cluster_info = json.loads(out)
    if "data" not in cluster_info:
        print( "Cluster " + cluster_id + " is not found.")
        return False

    cluster_info = cluster_info['data'][0]
    controller_info = cluster_info['NVControllerPublicIP'].split(":",1)

    with open('/root/sunlight_cloud_manager/scm/config.json') as file:
        clusters_config_data = json.load(file)

    if "AWS" in cluster_id:
        controller_ssh_port = 22
        controller_ssh_private_key_file="/root/sunlight_cloud_manager/scm/data/keypairs/manager_" + cluster_id
        infra_type="sunlight-on-aws"
    else:
        controller_ssh_port = clusters_config_data['sunlight']['clusters'][cluster_id]['ssh_port']
        controller_ssh_private_key_file="/root/.ssh/id_rsa"
        infra_type="sunlight-on-premise"

    sunlight_controller = dict(ansible_host=str(controller_info[0]), ansible_port=int(controller_ssh_port), ansible_user="root", ansible_ssh_private_key_file=controller_ssh_private_key_file, ansible_python_interpreter="/usr/bin/python2.7")

    infra_sunlight_controller = {
        "sunlight_controller": sunlight_controller
    }
    infra_variables = {
        "name": str(cluster_info['Name']),
        "cluster_id": cluster_id,
        "infrastructure_type": infra_type,
        "number_of_nexvisor_nodes": len(cluster_info['NVNodes'])
    }
    infra_hosts = {
        "hosts": infra_sunlight_controller,
        "vars": infra_variables
    }
    cluster_inventory = {
        "infrastructure": infra_hosts
    }
    print("Generating the inventory for cluster " + cluster_id)
    with open('infrastructure/' + cluster_id  + '.yml', 'w') as file:
        documents = yaml.dump(cluster_inventory, file)
    return True

def generate_deployment_playbook(cluster_id, recipe_id, directory):
    recipes = read_recipe_info()
    deploymentPlaybookFile = directory + cluster_id + "_" + recipe_id + "_deploy_playbook.yml"
    deploymentPlaybook = open(deploymentPlaybookFile, "w")
    # add the playbook to deploy the execution environment
    if "AWS" in cluster_id:
        execEnvPlaybook = "common-playbooks/deploy_exec_env_aws.yml"
    else:
        execEnvPlaybook = "common-playbooks/deploy_exec_env.yml"
    deploymentPlaybook.write("- import_playbook: ../" + execEnvPlaybook + "\n")
    # add the playbook to deploy the recipe
    if "playbook" in recipes[recipe_id]:
        recipePlaybook = recipes[recipe_id]['playbook']
        deploymentPlaybook.write("- import_playbook: ../" + recipePlaybook + "\n")

    if os.path.isfile(deploymentPlaybookFile):
        return deploymentPlaybookFile
    else:
        return None

def generate_cleanup_playbook(cluster_id, directory):
    cleanupPlaybookFile = directory + cluster_id + "_cleanup_playbook.yml"
    cleanupPlaybook = open(cleanupPlaybookFile, "w")
    removeAllPlaybook = "common-playbooks/remove_all.yml"
    cleanupPlaybook.write("- import_playbook: ../" + removeAllPlaybook + "\n")
    if os.path.isfile(cleanupPlaybookFile):
        return cleanupPlaybookFile
    else:
        return None

def select_execution_env(cluster_id, recipe_id):
    with open(r'infrastructure/' + cluster_id  + '.yml') as file:
        clusterInfo  = yaml.load(file, Loader=yaml.FullLoader)
    controller = clusterInfo['infrastructure']['hosts']['sunlight_controller']['ansible_host']
    ssh_port = clusterInfo['infrastructure']['hosts']['sunlight_controller']['ansible_port']
    ssh_key = clusterInfo['infrastructure']['hosts']['sunlight_controller']['ansible_ssh_private_key_file']
    ssh_user = clusterInfo['infrastructure']['hosts']['sunlight_controller']['ansible_user']
    cmd = "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i " + str(ssh_key) + " -p " + str(ssh_port) + " " + str(ssh_user) + "@" + str(controller) + " \"osd get_nodes --json\""
    out, err, rc = execute_local(cmd)
    clusterInfo = json.loads(out)
    if clusterInfo['result'] == "SUCCESS":
        nodeList = clusterInfo['data']
        numNodes = len(nodeList)
        minimum_cores = 1000
        minimum_nvmes = 100
        minimum_memory = 1000
        for node in nodeList:
            if node['total_cores'] < minimum_cores:
                minimum_cores = node['total_cores']

        # use different descriptor for AWS and on-prem cluster
        if "AWS" in cluster_id:
            infra_type = "sunlight-on-aws"
        else:
            infra_type = "sunlight-on-premise"
            
        # check if this recipe has recipe-specific execution environment descriptor
        if infra_type == "sunlight-on-aws":
            recipe_specific_exe_env_file = 'execution-environment/' + recipe_id + '.yml'
            recipe_specific_exe_env_file_aws = 'execution-environment/' + recipe_id + '_aws.yml'
            if os.path.isfile(recipe_specific_exe_env_file_aws):
                return recipe_specific_exe_env_file_aws
            elif os.path.isfile(recipe_specific_exe_env_file):
                return recipe_specific_exe_env_file
        elif infra_type == "sunlight-on-premise":
            recipe_specific_exe_env_file = 'execution-environment/' + recipe_id + '.yml' 
            if os.path.isfile(recipe_specific_exe_env_file):
                return recipe_specific_exe_env_file

        exeEnvIndexFile = "executionEnvironmentIndex.yml"
        if os.path.isfile(exeEnvIndexFile):
            with open(exeEnvIndexFile) as exeEnvIndexFileOpener:
                parsedExeEnv = yaml.load(exeEnvIndexFileOpener, Loader=yaml.FullLoader)
            exeEnvList = parsedExeEnv.get("executionEnvironmentIndex")
        else:
            print("Error: the execution environment index is missing.")
            return None

        exeEnvIndex = exeEnvList.keys()
        for exeEnv in sorted(exeEnvIndex):
            if ( infra_type == exeEnvList[exeEnv]['infra_type'] and minimum_cores >= int(exeEnvList[exeEnv]['minimum_cores_per_node']) and minimum_nvmes >= int(exeEnvList[exeEnv]['minimum_nvmes_per_node']) and minimum_memory >= int(exeEnvList[exeEnv]['minimum_memory_per_node']) ):
                return exeEnvList[exeEnv]['inventory']

        print("Error: no appropriate execution environment found.")
    return None

def update_recipeIndex():
    recipe_list = {}
    # check the recipes from each recipe hub
    for recipeHub in next(os.walk('./recipe-playbooks'))[1]:
        for recipe in next(os.walk('./recipe-playbooks/' + recipeHub))[1]:
            recipeMetadataFile = './recipe-playbooks/' + recipeHub + '/' + recipe + '/metadata.yml'
            # retrieve the recipe metadata
            if os.path.isfile(recipeMetadataFile):
                with open(recipeMetadataFile) as recipeMetadataFileOpener:
                    parsed_recipe_metadata = yaml.load(recipeMetadataFileOpener, Loader=yaml.FullLoader)
                    for recipe_id, recipe_data in parsed_recipe_metadata.items():
                        if "playbook" in recipe_data:
                            playbook = './recipe-playbooks/' + recipeHub + '/' + recipe + '/' + recipe_data["playbook"]
                            recipe_data["playbook"] = playbook
                        recipe_list[recipe_id] = recipe_data
                        # copy the exe env for this recipe to the execution-environment directory
                        recipe_exe_env_file = './recipe-playbooks/' + recipeHub + '/' + recipe + '/exe_env/' + recipe_id + '.yml'
                        recipe_exe_env_file_aws = './recipe-playbooks/' + recipeHub + '/' + recipe + '/exe_env/' + recipe_id + '_aws.yml'
                        if os.path.exists(recipe_exe_env_file):
                            if os.path.exists("./execution-environment/" + recipe_id + ".yml"):
                                os.rename("./execution-environment/" + recipe_id + ".yml", "./execution-environment/" + recipe_id + ".yml." + datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                            copyfile(recipe_exe_env_file, "execution-environment/" + recipe_id + ".yml" )
                        if os.path.exists(recipe_exe_env_file_aws):
                            if os.path.exists("./execution-environment/" + recipe_id + "_aws.yml"):
                                os.rename("./execution-environment/" + recipe_id + "_aws.yml", "./execution-environment/" + recipe_id + "_aws.yml." + datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                            copyfile(recipe_exe_env_file_aws, "execution-environment/" + recipe_id + "_aws.yml" )

    recipe_index = {}
    recipe_index["recipeIndex"] = recipe_list
    print("Updating the recipe index. ")
    with open('recipeIndex.yml', 'w') as file:
        documents = yaml.dump(recipe_index, file)
    return

