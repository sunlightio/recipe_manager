#!/bin/bash

#Execute as <Script> <RecipeID> <Image URL>
#N.B. need to test the args are valid
recipeID=$1
distro=$2
version=$3
imageURL=$4

imagename=`basename ${imageURL} | cut -d. -f1`

#First detect the template manager

CLUSTERID=`(cd /root/recipe_manager; ./recipeManager.py -ls deployment | grep -A 1 "${recipeID}" | awk 'NR > 1{print $2}')`
CLUSTER_IP=`cat /root/recipe_manager/infrastructure/${CLUSTERID}.yml | awk '/ansible_host/{print $2}'`
CLUSTER_PORT=`cat /root/recipe_manager/infrastructure/${CLUSTERID}.yml | awk '/ansible_port/{print $2}'`
TSERVER_IP=`cat /root/recipe_manager/deployment/${recipeID}/image_template_server.instance | python3 -mjson.tool | awk '/"ip"/ {gsub(/"|,/,"");print $2}'`

PFX1="ssh -i /root/.ssh/id_rsa root@${CLUSTER_IP} -p ${CLUSTER_PORT}"
PFX2="ssh -o StrictHostKeyChecking=no ubuntu@$TSERVER_IP"

echo "Step 1 - wget the image"
${PFX1} "${PFX2} wget -nvc ${imageURL}"
if [ $? -ne 0 ]; then
        echo "Failed to download image"
        exit
fi

echo "Step 2 - convert to qcow2"
${PFX1} "${PFX2} qemu-img convert -f raw -O qcow2 ${imagename}.raw ${imagename}.qcow2" 
if [ $? -ne 0 ]; then
        echo "Failed to convert image"
        exit
fi

echo "Step 4 - delete raw image file"
${PFX1} "${PFX2} sudo rm ${imagename}.raw"

echo "Step 3 - create template metadata"
${PFX1} "${PFX2} sudo mv ${imagename}.qcow2 /var/www/html/."
${PFX1} "${PFX2} 'cd /var/www/html && sudo ./local_template_server.py --provider AWS_AMI --image_filename ${imagename}.qcow2 --image_path ./ --disk_format qcow2 --os_distro ${distro} --os_version ${version} --min_ram 512 --add_template'"
