#!/bin/bash

#Execute as <Script> <AWS instance ID> <AWS region>
#N.B. need to test the args are valid
if [ -z "$1" -a -z "$2" -a -z "$3" ]
  then
    echo "Execute script as:"
    echo "$0 <AWS instance ID> <AWS region> <templateserver recipe ID>"
fi
instanceID=$1
region=$2
templateserver=$3
bucketID=`uuidgen`

ID=""
Bucket_created=0
Image_created=0
EXPORTID=""

instanceproductcode(){
        productCodes=`aws ec2 describe-instances --instance-ids=$instanceID | awk '/ProductCodes/ {gsub(/"|,/,"");print $2}'`
	platform=`aws ec2 describe-instances --instance-ids=$instanceID | awk '/Platform/ {gsub(/"|,/,"");print $2}'`
	if [ "$productCodes" != "[]" -o "$platform" == "windows" ];then
		echo "Instance $instanceID is a marketplace-based instance (contains AWS-licensed software) and is not exportable."
		exit
	fi

}

cleanup(){
	echo -n "Cleaning up and exiting..........................."
	
	if [ ${Image_created} -gt 0 ];then
	       aws ec2 deregister-image --image-id ${ID}
        fi	       
	if [ ${Bucket_created} -gt 0 ]; then
	    aws s3api delete-object --bucket ${bucketID} --key exports/${EXPORTID}.raw
		aws s3api delete-object --bucket ${bucketID} --key vmimportexport_write_verification
		out="nonempty"
		while [ "$out" != "" ]; do
			out=`aws s3 ls s3://${bucketID}`
			sleep 2
		done
	        aws s3api delete-bucket --bucket ${bucketID}
	fi
	echo "[done]"
	exit
}

echo "Params: [$instanceID] [$region] [$bucketID] [$templateserver]"

#check if the instance ID is exportable
instanceproductcode

#Setup region
echo -n "Setting region...................................."
aws configure set region ${region}
if [ $? -ne 0 ]; then
	echo "Failed to set region, check args [${region}]"
	exit
fi	
echo "[${region}]"

#Get image info
echo -n "Finding image metadata............................"
str=`aws ec2 describe-images --image-ids $(aws ec2 describe-instances --instance-ids ${instanceID} --query 'Reservations[0].Instances[0].ImageId' --output text) --query 'Images[0].Name'`
base=`basename $str 2>&1`
if [ $? -ne 0 ]; then
	base=$str
fi
distro=`echo ${base} | awk '{gsub(/"/,"");gsub(/-/," ");print $1}'`
if [ "${distro}" = "RHEL" ]; then
	version=`echo ${base} | awk '{gsub(/"/,"");gsub(/-|_/," ");print $2}'`	
elif [ "${distro}" = "ubuntu" ]; then
	version=`echo ${base} | awk '{gsub(/"/,"");gsub(/-|_/," ");print $3}'`
elif [ "${distro}" = "CentOS" ]; then
	version=`echo ${base} | awk '{gsub(/"/,"");gsub(/-|_/," ");print $3}'`
elif [ "${distro}" = "Fedora" ]; then
	version=`echo ${base} | awk '{gsub(/"/,"");gsub(/-|_/," ");print $3}'`
else
	distro="Unknown"
	version="1.0"
fi
echo "[done -> ${distro},${version}]"

#Create snapshot AMI of running instance
echo -n "Creating snapshot of the instance................."
ID=`aws ec2 create-image --instance-id ${instanceID} --name ${bucketID} --no-reboot | awk '/ImageId/ {gsub(/"|,/,"");print $2}'`
if [ $? -ne 0 -o -z ${ID} ]; then
        echo "Failed to create snapshot, check instance ID"
        cleanup
fi
Image_created=1
echo "[$ID]"
stop=0
echo -n "Waiting for the image to become available................."
while [ $stop -le 1 ]; do
        out=`aws --region=${region} ec2 describe-images --image-ids=${ID} | awk '/"State":/ {gsub(/"|,/,"");print $2}'`
	if [ "$out" != "available" ]; then
		sleep 10
        else
		stop=2
	fi
done
echo "[done]"

#Create bucket with unique uuid name
echo -n "Creating bucket..................................."
aws s3api create-bucket --bucket ${bucketID} --region ${region} --create-bucket-configuration LocationConstraint=${region} &> /dev/null
if ! [[ $(aws s3api list-buckets --query 'Buckets[?Name == `'$bucketID'`].[Name]' --output text) = ''$bucketID'' ]]; then
    echo "Failed to create bucket, check aws account"
    cleanup
fi
Bucket_created=1
echo "[$bucketID]"

#Export snapshot as raw disk to S3 bucket
echo -n "Exporting snapshot as raw disk...................."
EXPORTID=`aws ec2 export-image --image-id ${ID} --disk-image-format RAW --s3-export-location S3Bucket=${bucketID},S3Prefix=exports/ | awk '/ExportImageTaskId/ {gsub(/"|,/,"");print $2}'`
if [ $? -ne 0 -o -z ${EXPORTID} ]; then
	echo "Failed to start snapshot export"
        cleanup
fi
echo "[${EXPORTID}]"

#Now loop until export completes
stop=0
echo "Waiting for export to complete: This may take few minutes depending on the size of the image...."
count=0
while [ $stop -le 1 ]; do
	out=`aws ec2 describe-export-image-tasks --export-image-task-ids ${EXPORTID} | awk '/"Status":/ {gsub(/"|,/,"");print $2}'`
	progress=`aws ec2 describe-export-image-tasks --export-image-task-ids ${EXPORTID} | awk '/"Progress":/ {gsub(/"|,/,"");print $2}'`
	message=`aws ec2 describe-export-image-tasks --export-image-task-ids ${EXPORTID} | awk '/"StatusMessage":/ {gsub(/"|,/,"");print $2}'`
	if [ $? -ne 0 -o -z ${out} ]; then
		echo "Failed to retrieve export status [${out}][$?]"
		cleanup
	fi

	if [ "$out" = "completed" ]; then 
		stop=256
		#echo "Done"
	elif [ "$out" != "active" ]; then
		echo "Export failed [${out}]"
		cleanup
	else
		#check again the s3 bucket existence 
		if ! [[ $(aws s3api list-buckets --query 'Buckets[?Name == `'$bucketID'`].[Name]' --output text) = ''$bucketID'' ]]; then
			echo "S3 bucket failed, please check the aws account and try again"
                        cleanup
                fi
		echo "status : [${out}] progress : [$progress] statusMessage: [$message]"
	        sleep 10
		count=`expr $count + 1`
	fi

	if [ ${count} -eq 60 ]; then
		echo "Please wait ...do not abort"
		count=0
	fi
done
echo "[done]"

#Make image publicly accessible
echo -n "Make image publicly accessible...................."
aws s3api put-object-acl --bucket ${bucketID} --key exports/${EXPORTID}.raw --acl public-read &> /dev/null
if [ $? -ne 0 ]; then
        echo "Failed to make bucket public, check aws account"
        exit
fi
echo "[done]"
echo "Conversion completed. Raw image available now being added to template server"

echo "Starting now to download the raw image into the template server...................."
./template_convert.sh ${templateserver} ${distro} ${version} https://${bucketID}.s3.${region}.amazonaws.com/exports/${EXPORTID}.raw
echo "[done]"
#Finally cleanup and delete the bucket and snapshot image
cleanup
