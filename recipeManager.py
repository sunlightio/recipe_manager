#!/usr/bin/python3
import json
import yaml
from subprocess import Popen, PIPE
import argparse
import subprocess
import time
import uuid
import os.path
from helpers import *

def list_cluster():
    cmd = "curl -X GET http://localhost:8383/clusters"
    out, err, rc = execute_local(cmd)
    clusters = json.loads(out)
    clusters = clusters['data']
    print("The following clusters are managed by this SIM:")
    for cluster in clusters:
        print("------------")
        print("Cluster ID: " + cluster['NVClusterID']  + "\n    Name: " + cluster['Name'])
        if "AWS" in cluster['NVClusterID']:
            if cluster['NVNodes']:
                print("    Region: " + cluster['NVNodes'][0]['ProviderAvailabilityZone'])
        print("    State: " + cluster['State'] )
    return

def list_recipe():
    recipes = read_recipe_info()
    if recipes is None:
        print("The recipes cannot be loaded. Please update the recipe hub.")
        return

    print("The following recipes are available on this SIM:")
    for recipe_id, recipe_info in recipes.items():
        print("------------")
        print("Recipe ID: " + recipe_id + "\n    Description: " + recipe_info.get("description"))
        if "dependent recipe" in recipe_info:
            print("    Dependent recipe: " + recipe_info.get("dependent recipe"))
        if "tag" in recipe_info:
            print("    Tag option(s): ")
            for tag_name, tag_data in recipe_info.get("tag").items():
                print("        " + tag_name + " - " + tag_data.get("description"))
                if "dependent recipe" in tag_data:
                    print("           Dependent recipe:" + tag_data.get("dependent recipe") )
    return

def list_deployment():
    deploymentInfo = read_deployment_info()
    if deploymentInfo is None:
        print("There isn't any deployment existing.")
        return
    if not deploymentInfo['deploymentList']:
        print("There isn't any deployment existing.")
    else:
        print("The following deployments are managed by this SIM:\n--------------")
        for deployment in deploymentInfo['deploymentList']:
            print("Deployment ID: " + str(deployment['id']) + "\n    Cluster: " + deployment['cluster_id'] + "\n    Recipe: " + deployment['recipe_id'] + "\n    Status: " + deployment['status'] + "\n--------------")
    return

def list_recipe_hub():
    for i in next(os.walk('./recipe-playbooks'))[1]:
        print(i)
    return

def update_recipe(recipe_hub_name, recipe_hub_branch):
    recipe_hub_dir = "recipe-playbooks/" + recipe_hub_name
    if os.path.isdir(recipe_hub_dir):
        cmd = "cd " + recipe_hub_dir + "; git pull origin " + recipe_hub_branch
        out, err, rc = execute_local(cmd)
        if rc != 0:
            print(err)
        else:
            print(out)
            update_recipeIndex()
    else:
        print("The recipe hub " + recipe_hub_name + " is not found.")
    return

def cleanup(cluster_id):
    inventoryReady = generate_infrastructure_inventory(cluster_id)
    if inventoryReady is False:
        return

    print("Cleaning the cluster " + cluster_id)

    cleanupPlaybookFile = generate_cleanup_playbook(cluster_id, deploymentDataDir)
    update_deployment_status_by_cluster(cluster_id, "Deleting")

    ansibleRunWithArgs = ['ansible-playbook', '-i', 'infrastructure/' + cluster_id + '.yml', cleanupPlaybookFile]

    if verbosityLevel == 2:
        ansibleRunWithArgs.append('-vvv')

    logfile = openLogFile(cluster_id)
    if verbosityLevel >= 1:
        process = subprocess.Popen(ansibleRunWithArgs,
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE,
                     universal_newlines=True)
        while True:
            output = process.stdout.readline()
            if output:
                print(output.strip())
                logfile.write(output)
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                if return_code != 0:
                    for error in process.stderr.readlines():
                        print(error)
                    update_deployment_status_by_cluster(cluster_id, "Error")
                else:
                    remove_all_deployment(cluster_id)
                # Process has finished, read rest of the output
                for output in process.stdout.readlines():
                    print(output.strip())
                break
        return
    else:
        process = subprocess.Popen(['python3', 'runPlaybook.py', cluster_id, cleanupPlaybookFile],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)
        time.sleep(1)
        print("The cluster is being cleaned in the background.")
        return

def deploy_recipe(recipe_id, cluster_id, tag="", limit=""):
    recipes = read_recipe_info()
    print("Recipe: " + recipes[recipe_id]['description'] )

    generate_infrastructure_inventory(cluster_id)
    deploymentInfo = read_deployment_info()
    if deploymentInfo is None:
        deploymentInfo = {}
        deploymentInfo['deploymentList'] = []

    dependentDeploymentID = ""
    if "dependent recipe" in recipes[recipe_id]:
        dependentRecipe = recipes[recipe_id]["dependent recipe"]
        for deployment in deploymentInfo['deploymentList']:
            if deployment['recipe_id'] == dependentRecipe and deployment['cluster_id'] == cluster_id:
                dependentDeploymentID = deployment['id']
                print("The dependent recipe " + dependentRecipe + " is found in deployment.")
                dependentDeploymentFound = True
                break
        if not dependentDeploymentFound:
            print("The dependent recipe " + dependentRecipe + " is not found in deployment.")
            return


    exeEnvInventory = select_execution_env(cluster_id, recipe_id)
    if exeEnvInventory is None:
        return
    print("The execution environment is defined in " + exeEnvInventory)

    deploymentPlaybookFile = generate_deployment_playbook(cluster_id, recipe_id, deploymentDataDir)

    if tag:
        if "tag" in recipes[recipe_id]:
            if tag in recipes[recipe_id]["tag"]:
                deploymentTag = tag
                compulsory_tags = recipes[recipe_id]["tag"][tag]["compulsory_tags"]
                if compulsory_tags is not None:
                    deploymentTag = tag + ',' + compulsory_tags
                print("The tags applied are:" + deploymentTag)
                if "dependent recipe" in recipes[recipe_id]["tag"][tag]:
                    dependentRecipe = recipes[recipe_id]["tag"][tag]["dependent recipe"]
                    for deployment in deploymentInfo['deploymentList']:
                        if deployment['recipe_id'] == dependentRecipe and deployment['cluster_id'] == cluster_id:
                            dependentDeploymentID = deployment['id']
                            print("The dependent recipe " + dependentRecipe + " is found in deployment " + dependentDeploymentID + ".")
                            dependentDeploymentFound = True
                            break
                    if not dependentDeploymentFound:
                        print("The dependent recipe " + dependentRecipe + " is not found in deployment.")
            else:
                print("The tag provided is not supported in the recipe.")
                return
    else:
        deploymentTag = ""

    if limit:
        #TODO: check exist of the given host
        limitedHost = limit
    else:
        limitedHost = ""

    deploymentID = str(uuid.uuid4())

    thisDeployment = {
        "id": deploymentID,
        "cluster_id": cluster_id,
        "recipe_id": recipe_id,
        "exe_env": exeEnvInventory,
        "status": "deploying"
    }
    deploymentInfo['deploymentList'].append(thisDeployment)
    with open('deployment.json', 'w') as deploymentFile:
        json.dump(deploymentInfo, deploymentFile, sort_keys=True, indent=4)

    logfile = openLogFile(cluster_id)

    ansibleRunWithArgs = ['ansible-playbook', '-i', 'infrastructure/' + cluster_id + '.yml', '-i', exeEnvInventory, deploymentPlaybookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID]

    if verbosityLevel == 2:
        ansibleRunWithArgs.append('-vvv')

    if verbosityLevel >= 1:
        if not deploymentTag:
            if not limitedHost:
                process = subprocess.Popen(ansibleRunWithArgs,
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE,
                     universal_newlines=True)
            else:
                process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + cluster_id + '.yml', '-i', exeEnvInventory, deploymentPlaybookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID, '-l', limitedHost + ',sunlight_platform,infrastructure' ],
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE,
                     universal_newlines=True)

        else:
            if not limitedHost:
                process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + cluster_id + '.yml', '-i', exeEnvInventory, deploymentPlaybookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID, '--tags', deploymentTag ],
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE,
                     universal_newlines=True)
            else:
                process = subprocess.Popen(['ansible-playbook', '-i', 'infrastructure/' + cluster_id + '.yml', '-i', exeEnvInventory, deploymentPlaybookFile, '--extra-vars', 'deployment_id=' + deploymentID + ' dependent_deployment_id=' + dependentDeploymentID, '--tags', deploymentTag ,'-l', limitedHost + ',sunlight_platform,infrastructure'],
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE,
                     universal_newlines=True)

        while True:
            output = process.stdout.readline()
            if output:
                print(output.strip())
                logfile.write(output)
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                if return_code != 0:
                    print(deploymentID)
                    update_deployment_status(deploymentID, "Error")

                    if process.stderr is not None:
                        for error in process.stderr.readlines():
                            logfile.write(error)
                    # Process has finished, read rest of the output
                    for output in process.stdout.readlines():
                        logfile.write(output)
                else:
                    update_deployment_status(deploymentID, "Completed")
                break
    else:
        process = subprocess.Popen(['python3', 'runPlaybook.py', cluster_id, deploymentPlaybookFile, deploymentTag, deploymentID, exeEnvInventory, dependentDeploymentID, limitedHost],
                     stdout=logfile,
                     stderr=logfile,
                     universal_newlines=True)
        time.sleep(1)
        print("The cluster is being deployed in the background.")

    return

def parse_main(args):
    global deploymentDataDir
    deploymentDataDir = "deployment/"
    global verbosityLevel
    verbosityLevel = args.verbose

    if args.list is not None:
        if args.list == "cluster":
            list_cluster()
            return
        elif args.list == "recipe":
            list_recipe()
            return
        elif args.list == "deployment":
            list_deployment()
            return
        elif args.list == "recipe-hub":
            list_recipe_hub()
            return    

    if args.update is not None:
        if args.update[0] == "recipe":
            update_recipe(args.update[1], args.update[2])
        return

    if args.deploy is not None:
        if args.tag is None:
            args.tag = ""
        if args.limit is None:
            args.limit = ""
        deploy_recipe(args.deploy[0], args.deploy[1], args.tag, args.limit)
        return

    if args.cleanup is not None:
        cleanup(args.cleanup)
        return

    print("Please check the help info using --help")
    return

def parse_args():
    """
    Parse cmdline args.
    """
    parser = argparse.ArgumentParser(description="Sunlight Recipe Manager")
    parser.add_argument('-ls', '--list', choices=['cluster', 'recipe', 'deployment', 'recipe-hub'], dest='list', required=False, help="show a list of Sunlight clusters/recipes/deployments/recipe-hubs managed by the SIM")
    parser.add_argument('-d', "--deploy", metavar=('[recipe-id]', '[cluster-id]'), dest='deploy', nargs=2, required=False, help="execute the recipe against the designated cluster")
    parser.add_argument('-t', "--tag", dest='tag', metavar='[tag]', required=False, help="Optional - specify a tag for a deployment")
    parser.add_argument('-l', "--limit", dest='limit', metavar='[limit-host]', required=False, help="Optional - limit to the given host for a deployment")
    parser.add_argument('-u', "--update", metavar=('recipe', '[recipe-hub-name]', '[recipe-hub-branch]'), dest='update', nargs=3, required=False, help="update the recipes from a specific branch of a given recipe hub")
    parser.add_argument('-c', "--cleanup", metavar='[cluster-id]', dest='cleanup', required=False, help="remove all the VMs and configurations on this Sunlight cluster to restore to a fresh environment for deployment other recipes.")
    parser.add_argument('-v', "--verbose", dest='verbose', action='count', default=0, required=False, help="show the level of details in the foreground while the deployment is running, -v for foreground message, -vv for extra Ansible message")
    parser.set_defaults(func=parse_main)

    args = parser.parse_args()
    args.func(args)


# Main:
parse_args()
