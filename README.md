# Sunlight Recipe Manager #

The recipe manager is introduced to help configure the resources on the Sunlight platform to have the best performance and deploy the different applications to consume the performance offered by Sunlight platform. Each recipe consists of configuring the execution environment, i.e. CPU cores, NVMe storage datastores, network objects in an optimal setup for different applications, and deploying these applications, even starting some workloads for the applications. The recipe manager would check the resources available on a given cluster and choose the best execution environment configuration for a given application.

### Usage of Recipe Manager ###

The process to deploy an application on the Sunlight platform using the recipe manager could be summarised as follows.  

*  Deploy a SIM (Sunlight Infrastructure Manager) on AWS if you haven’t. 

*  Add your on-premise cluster to the SIM or create an AWS cluster on the SIM. 

*  Access the command shell of the SIM using the SSH key which was added to the SIM instance when it was deployed. 

```
# ssh -i aws_manager/data/keypairs/aws_manager_rsa ubuntu@123.123.123.123 
```

*  Install Ansible on SIM as follows.

```
# sudo apt update
# sudo apt install software-properties-common
# sudo apt-add-repository --yes --update ppa:ansible/ansible
# sudo apt install ansible
```

*  Download the recipe manager which contains a CLI-based recipe manager tool and all the Ansible playbooks to configure the Sunlight platform and deploy the applications. 
    *  Download the recipe manager: 

```
# cd /root/
# git clone https://bitbucket.org/sunlightio/recipe_manager.git
```

*  Go to the "recipe_manager/recipe-playbooks" directory of the recipe manager and add the git repository of the recipe hub as follows.

```
# cd recipe_manager/recipe-playbooks/
# git clone https://bitbucket.org/sunlightio/sunlight-recipe-hub.git 
```

*  Update and add the recipes from a recipe hub, such as [Sunlight Recipe Hub](https://bitbucket.org/sunlightio/sunlight-recipe-hub) as follows.

```
# cd /root/recipe_manager/
# ./recipeManager.py --update recipe sunlight-recipe-hub master
```
 
*  Run the recipe manager tool to manage the deployment. 

*  The recipes available on the recipe manager would be updated regularly to support more applications.  
    *  Run “git pull” in the recipe manager directory to update the local recipe manager to the latest version. 

### Recipe Manager CLI

Go to the directory where the recipe manager was downloaded. Run the recipe manager:
```
# recipeManager.py --help
usage: recipeManager.py [-h] [-ls {cluster,recipe,deployment,recipe-hub}]
                        [-d [recipe-id] [cluster-id]] [-t [tag]]
                        [-l [limit-host]] [-u recipe [recipe-hub-name]
                        [recipe-hub-branch]] [-c [cluster-id]] [-v]

Sunlight Recipe Manager

optional arguments:
  -h, --help            show this help message and exit
  -ls {cluster,recipe,deployment,recipe-hub}, --list {cluster,recipe,deployment,recipe-hub}
                        show a list of Sunlight clusters/recipes/deployments/recipe-hubs managed by the SIM
  -d [recipe-id] [cluster-id], --deploy [recipe-id] [cluster-id]
                        execute the recipe against the designated cluster
  -t [tag], --tag [tag]
                        Optional - specify a tag for a deployment
  -l [limit-host], --limit [limit-host]
                        Optional - limit to the given host for a deployment
  -u recipe [recipe-hub-name] [recipe-hub-branch], --update recipe [recipe-hub-name] [recipe-hub-branch]
                        update the recipes from a specific branch of a given recipe hub
  -c [cluster-id], --cleanup [cluster-id]
                        remove all the VMs and configurations on this Sunlight cluster to restore to a fresh environment for deploying other recipes.
  -v, --verbose         show the details in the foreground while the deployment is running
```

The recipe manager has the following parameters available.

| Short | Full | Description of the parameter |
| ------------ | ------------ | ---------- |
| -h | --help | show this help message and exit |
| -ls | --list {cluster,recipe,deployment} | show a list of Sunlight clusters/recipes/deployments managed by the SIM. |
| -d | --deploy [recipe-id] [cluster-id] | execute the recipe against the designated cluster. |
| -u | --update | update the recipes from a specific branch of a given recipe hub. |
| -c | --cleanup | remove all the VMs and configurations on this Sunlight cluster to restore to a fresh environment for deploying other recipes. |
| -v | --verbose | show the details in the foreground while the deployment is running. |

Step 1 - List available clusters added to this SIM
```
user@ipaddress:~/platform_scripts/recipe_manager# ./recipeManager.py --list cluster
The following clusters are managed by this SIM:
------------
Cluster ID: SL-7539850186079518
      Name: Altos-R385
      State: Online
 ------------
 Cluster ID: SL-5862251411051901
       Name: Dev cluster
      State: Offline
 ------------
 Cluster ID: SL-6025686704595228
       Name: CBG demo111
      State: Online
 ------------
```

Step 2 - List available recipes
```
user@ipaddress:~/platform_scripts/recipe_manager# ./recipeManager.py --list recipe
The following recipes are available on this SIM:
------------
Recipe ID: benchmark-tools
    Description: Deploy benchmark tools including fio and vdbench
------------
Recipe ID: cloudera
    Description: Deploy CDP Private Cloud Base
------------
Recipe ID: fio-workload
    Description: Deploy Fio-based workload to drive maximum IOPs
------------
```

Step 3 - Deploy recipe
```
user@ipaddress:~/platform_scripts/recipe_manager# ./recipeManager.py --deploy fio SL-6025686704595228
Recipe: Deploy Fio-based max iops demo workload
Generating the inventory for cluster SL-6025686704595228
The cluster is being deployed in the background.
```

Step 4 - Show deployment status
```
root@ipaddress:~/platform_scripts/recipe_manager# ./recipeManager.py --list deployment
The following deployments are managed by this SIM:
--------------
Deployment ID: a50c0dc5-c86a-4fbb-80c0-bb1d69280362
      Cluster: SL-6025686704595228
       Recipe: fio
       Status: deploying
--------------
```

Step 5 - Cleanup and reset cluster to factory default
```
root@ipaddress:~/platform_scripts/recipe_manager# ./recipeManager.py --cleanup SL-6025686704595228
The following deployments are managed by this SIM:
--------------
Deployment ID: 94538a6d-6305-4acc-afdf-9132ebc51096
      Cluster: SL-6025686704595228
       Recipe: fio
       Status: Deleting
--------------
```

### More Information ###

*  https://performance.sunlight.io/ 
*  http://docs.sunlight.io/
*  https://bitbucket.org/sunlightio/sunlight-recipe-hub


